package kdbttp

import (
	_ "github.com/mattn/go-sqlite3"
)

type DbResult struct {
	Err    error
	Names  []string
	Types  []string
	Values []string
}

type DbSession interface {
	Open(uri string) bool
	Close()
	GetValues(container string, vals []string, keys map[string]string) *DbResult
	SetValues(container string, vals map[string]string, keys map[string]string) *DbResult
	DelValues(container string, vals []string, keys map[string]string) *DbResult
	HasValues(container string, vals []string, keys map[string]string) *DbResult
}
