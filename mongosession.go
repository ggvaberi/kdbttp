package kdbttp

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoSession struct {
	host   string
	user   string
	pass   string
	client *mongo.Client
	Failed bool
}

// func (self MongoSession) Open(addr string, user string, pass string) bool {
func (self *MongoSession) Open(uri string) bool {
	//url := proto + addr + ":" + strconv.Itoa(int(port)) + "/dbopen"
	data := strings.Split(uri, ":")

	self.host = data[0]
	self.user = data[1]
	self.pass = data[2]

	url := "mongodb+srv://" + self.user + ":" + self.pass + "@" + self.host + ".hr2fsad.mongodb.net/?retryWrites=true&w=majority"

	log.Print("Open database session: ", url)

	serverAPI := options.ServerAPI(options.ServerAPIVersion1)

	opts := options.Client().ApplyURI(url).SetServerAPIOptions(serverAPI)

	var err error

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	self.client, err = mongo.Connect(ctx, opts)

	//self.client, err = mongo.Connect(context.TODO(), opts)

	if err != nil {
		log.Println("MongoDB error: ", err.Error())
		return false
	}

	log.Print("Open database connection pass: pinging...")

	if err := self.client.Database("admin").RunCommand(context.TODO(), bson.D{{"ping", 1}}).Err(); err != nil {
		log.Println("MongoDB error: ", err.Error())
		self.client = nil
		return false
	}

	return true
}

func (self *MongoSession) Close() {
	if self.client == nil {
		log.Println("MongoDB client invalid.")
		return
	}

	if err := self.client.Disconnect(context.TODO()); err != nil {
		log.Println("MongoDB error: ", err.Error())
	}

	self.client = nil
	self.user = ""
	self.pass = ""
	self.host = ""
}

func (self *MongoSession) UpdateSession() bool {
	url := "mongodb+srv://" + self.user + ":" + self.pass + "@" + self.host

	var err error

	if self.client != nil {
		err = self.client.Database("admin").RunCommand(context.TODO(), bson.D{{"ping", 1}}).Err()

		if err == nil {
			return true
		}

		log.Println("MongoDB update session error: ", err.Error())

		self.client = nil
	}

	serverAPI := options.ServerAPI(options.ServerAPIVersion1)

	opts := options.Client().ApplyURI(url).SetServerAPIOptions(serverAPI)

	self.client, err = mongo.Connect(context.TODO(), opts)

	if err != nil {
		log.Println("MongoDB update error: ", err.Error())
		self.client = nil
		return false
	}

	return true
}

func (self *MongoSession) ValidSession() bool {
	var err error

	if self.client == nil {
		log.Println("MongoDB client invalid.")
		return false
	}

	if err = self.client.Database("admin").RunCommand(context.TODO(), bson.D{{"ping", 1}}).Err(); err != nil {
		log.Println("MongoDB validity error: ", err.Error())
		return false
	}

	return true
}

func (self *MongoSession) GetValues(doc string, vals []string, keys map[string]string) *DbResult {
	log.Println("MongoDB find collection: ", doc)

	result := new(DbResult)

	if self.client == nil {
		log.Println("MongoDB client invalid.")
		result.Err = errors.New("Invalid session.")
		return result
	}
	col := self.client.Database("mongo13_db").Collection(doc)

	var filter bson.D

	for k, v := range keys {
		kk := k //"\"" + k + "\""
		vv := v //"\"" + v + "\""
		filter = append(filter, bson.E{kk, vv})
	}

	//filter = bson.D{{"email", "user@mail.com"}}
	log.Println("MongoDB find filter: ", filter)
	//opts := options.Find().SetSort(bson.D{{}})
	//cur, err := col.Find(context.TODO(), filter, nil) //opts) bson.D{{"name", "Bob"}}
	cur, err := col.Find(context.TODO(), filter, nil) //opts) bson.D{{"name", "Bob"}}

	if err != nil {
		log.Println("MongoDB find error: ", err.Error())
		result.Err = err
		return result
	}

	var res []bson.M

	if err = cur.All(context.TODO(), &res); err != nil {
		log.Println("MongoDB cursor error: ", err.Error())
		result.Err = err
		return result
	}

	var ret []string

	log.Println("MongoDB cursor result: ", res)

	for _, r := range res {
		for n := range vals {
			d := fmt.Sprintf("%v", r[vals[n]])

			ret = append(ret, d)
		}
	}

	log.Println("GetValues result: ", ret)
	self.Failed = false

	result.Values = ret
	return result
}

func (self *MongoSession) SetValues(doc string, vals map[string]string, keys map[string]string) *DbResult {
	log.Println("MongoDB insert collection: ", doc)

	result := new(DbResult)

	if self.client == nil {
		log.Println("MongoDB client invalid.")
		result.Err = errors.New("Invalid session.")
		return result
	}

	col := self.client.Database("mongo13_db").Collection(doc)

	var filter bson.D

	var replace bson.D

	for k, v := range vals {
		kk := k //"\"" + k + "\""
		vv := v //"\"" + v + "\""
		filter = append(filter, bson.E{kk, vv})
	}

	//filter = bson.D{{"email", "user@mail.com"}}
	log.Println("MongoDB insert filter: ", filter)
	//opts := options.Find().SetSort(bson.D{{}})
	//cur, err := col.Find(context.TODO(), filter, nil) //opts) bson.D{{"name", "Bob"}}
	if keys != nil && len(keys) > 0 {
		for k, v := range keys {
			replace = append(replace, bson.E{k, v})
		}

		update := bson.D{{"$set", filter}}

		log.Println("MongoDB replace: ", replace)

		//res, err := col.ReplaceOne(context.TODO(), replace, filter, nil)
		res, err := col.UpdateOne(context.TODO(), replace, update, nil)

		if err != nil {
			log.Println("MongoDB replace error: ", err.Error())
			result.Err = err
			return result
		}

		log.Println("MongoDB replace count: ", res.MatchedCount)
	} else {
		res, err := col.InsertOne(context.TODO(), filter, nil)
		if err != nil {
			log.Println("MongoDB insert error: ", err.Error())
			result.Err = err
			return result
		}

		log.Println("MongoDB insert id: ", res.InsertedID)
	}

	return result
}

func (self *MongoSession) DelValues(doc string, vals []string, keys map[string]string) *DbResult {
	log.Println("MongoDB delete collection: ", doc)

	result := new(DbResult)

	if self.client == nil {
		log.Println("MongoDB client invalid.")
		result.Err = errors.New("Invalid session.")
		return result
	}

	col := self.client.Database("mongo13_db").Collection(doc)

	var err error

	var filter bson.D

	for k, v := range keys {
		filter = append(filter, bson.E{k, v})
	}

	log.Println("MongoDB delete filter: ", filter)

	res, err := col.DeleteOne(context.TODO(), filter, nil)

	if err != nil {
		log.Println("MongoDB delete error: ", err.Error())
		result.Err = err
		return result
	}

	log.Println("MongoDB delete count: ", res.DeletedCount)

	return result
}

func (self *MongoSession) HasValues(doc string, vals []string, keys map[string]string) *DbResult {
	log.Println("MongoDB check find collection: ", doc)
	log.Println("MongoDB check find collection val: ", vals)
	log.Println("MongoDB check find collection key: ", keys)

	result := new(DbResult)

	if self.client == nil {
		log.Println("MongoDB client invalid.")
		result.Err = errors.New("Invalid session.")
		return result
	}

	col := self.client.Database("mongo13_db").Collection(doc)

	var filter bson.D

	for k, v := range keys {
    //    vv := "\"" + v + "\""
    //    kk := "\"" + k + "\""
		filter = append(filter, bson.E{k, v})
	}

	log.Println("MongoDB check find filter: ", filter)

	var dres map[string]string

	err := col.FindOne(context.TODO(), filter, nil).Decode(&dres)

	if err != nil {
		log.Println("MongoDB check find error: ", err.Error())
		result.Err = err
		return result
	}

  log.Println("MongoDB check find result: ", dres)

	for _, v := range vals {
		_, ok := dres[v]

		if !ok {
			log.Println("MongoDB check find error: Absent value")
			result.Err = errors.New("Value absent.")
			return result
		}
	}

	return result
}
